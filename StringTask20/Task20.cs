﻿using System;
using System.Linq;

/* 
 * 20. В строке между словами вставить вместо пробела запятую и пробел. 
 */

namespace StringTask20
{
    class Task20
    {
        static void Main(string[] args)
        {
            const string myString = " fds fds fdds  da as , fff ";
            Console.WriteLine("Source string: \"{0}\"", myString);
            Console.WriteLine("New string: \"{0}\"", CreateNewString(myString));
            Console.WriteLine("New string: \"{0}\"", CreateNewNewString(myString,' ', ", "));
            
        }

        private static string CreateNewString(string s)
        {
            var words = s.Split(' ').ToList();
            var wordsForDelete = words.ToList();
            foreach (var a in words.Where(a => a == ""))
            {
                wordsForDelete.Remove(a);
            }

            /*
            foreach (string a in words)
            {
                if (a == "")
                {
                    wordsForDelete.Remove(a);
                }
            }
            */

            return string.Join(", ", wordsForDelete.ToArray());
        }

        private static string CreateNewNewString(string s, char from, string to)
        {
            return string.Join(to, s.Split(from).Where(x => x != "").ToArray());
        }
    }
}
